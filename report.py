# -*- coding: utf-8 -*-
from trytond.report import Report

__all__ = ['ReporteVisitas']

class ReporteVisitas(Report):
    __name__ = 'agentes.reporte.visitas'
    @classmethod
    def get_context(cls, records, data):
      report_context = super(ReporteVisitas, cls).get_context(records, data)      
      return report_context