# health_health_agents_fiuner-3.2.0

GNU Health health agents (fiuner) module
########################################

This module add support to:

     * Register health agents activities on domiciliary visits
     * Print activities on a report
