# -*- coding: utf-8 -*-


from trytond.model import ModelView, ModelSingleton, ModelSQL, fields


__all__ = ['AgentesVisitas']

class AgentesVisitas(ModelSQL,ModelView):
    'Visitas de agentes'
    __name__ = 'agentes.visitas'

    agente = fields.Many2One('gnuhealth.healthprofessional', 'Profesional de Salud', required=True)
    
    fecha_visita = fields.Date('Fecha de la Visita', required=True, help="Fecha en la que se realizo la Visita")
    
    unidad_visitada = fields.Many2One('gnuhealth.du', 'Unidad Domiciliaria')

    tipo_unidad = fields.Selection([
        (None, ''),
        ('Institucion Educativa', 'Institucion Educativa'),
        ('Institucion Deportiva', 'Institucion Deportiva'),
        ('Domicilio Particular', 'Domicilio Particular'),
        ('CAPS D Angelo', 'CAPS D Angelo'),
        ('SUM', 'SUM'),
	('Lugar Público', 'Lugar Publico'),
        ('Otros', 'Otros'),
        ], 'Tipo de Unidad', sort=False)

    barrio = fields.Many2One('gnuhealth.operational_sector', 'Barrio', 
			     help="Barrio donde se encuentra la Unidad domiciliaria visitada")
    
    motivo= fields.Char('Motivo Visita')

    observaciones=fields.Text('Observaciones', 
			      help= "Descripcion de las tareas desarrolladas"
			             " o cualquier comentario que se crea necesario")