# -*- coding: utf-8 -*-

from trytond.pool import Pool
from .agentes import *
from .report import *

def register():
  Pool.register(
    AgentesVisitas,
    module='health_health_agents_fiuner', type_='model')
  Pool.register(
    ReporteVisitas,
    module='health_health_agents_fiuner', type_='report')
